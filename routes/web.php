<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false,
]);

Route::get('/logout', 'Auth\LoginController@logout')
    ->name('get.logout');

Route::middleware(['auth'])->group(function (){
    Route::group([
        'prefix'=>'personal'
    ], function (){
        Route::get('/orders', 'PeronalOrderController@index')
            ->name('personal.orders.index');
        Route::get('/orders/{order}', 'PeronalOrderController@show')
            ->name('personal.orders.show');
    });

    Route::group(['prefix' => 'admin'], function () {
        Route::group(['middleware' => 'is_admin'], function () {
            Route::get('/orders', 'OrderController@index')
                ->name('orders.index');
            Route::get('/orders/{order}', 'OrderController@show')
                ->name('orders.show');
        });
        Route::resource('categories', 'CategoryController');
        Route::resource('products', 'ProductController');

    });
});




Route::get('/', 'MainController@index')
    ->name('main.index');


Route::post('/basket/add/{id}', 'BasketController@basketAdd')
    ->name('basket.add');

Route::group(['middleware' => 'basket_not_empty'], function () {
    Route::get('/basket', 'BasketController@basket')
        ->name('basket.basket');
    Route::get('/basket/order', 'BasketController@order')
        ->name('basket.order');
    Route::post('/basket/confirm', 'BasketController@confirm')
        ->name('basket.confirm');
    Route::post('/basket/remove/{id}', 'BasketController@basketRemove')
        ->name('basket.remove');
});

Route::get('/categories', 'MainController@categories')
    ->name('main.categories');
Route::get('/{category}', 'MainController@category')
    ->name('main.category');

Route::get('/{category}/{product?}', 'MainController@product')
    ->name('main.product');



