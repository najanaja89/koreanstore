<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Korean Store: @yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    {{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('css/starter-template.css') }}" rel="stylesheet">
</head>
<body>
<div class="container text-center" align="center" style="">
    <div class="row" style="background-image:url('{{asset('header.png')}}')">
        <div class="row"  style="margin-top: 50px">
            <img src="{{asset('korean_store.png')}}" alt="">
        </div>
        <div class="row" style="margin-bottom: 50px">
            <img src="{{asset('gadgets & lifestyle.png')}}" alt="">
        </div>
    </div>
</div>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{route('main.index')}}">Korean Store</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li @routeactive('main.index')><a href="{{route('main.index')}}">Все товары</a></li>
                <li @routeactive('main.categories')><a href="{{route('main.categories')}}">Категории</a>
                </li>
                <li @routeactive('basket.basket')><a href="{{route('basket.basket')}}">В корзину</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @guest
                    <li><a href="{{ route('login') }}">Войти</a></li>
                @endguest

                @auth
                    @admin
                    <li><a href="{{ route('orders.index') }}">Админка</a></li>
                @else
                    <li><a href="{{ route('personal.orders.index') }}">Мои заказы</a></li>
                    @endadmin
                    <li><a href="{{ route('get.logout') }}">Выйти</a></li>
                @endauth

            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="starter-template">
        @if(session()->has('success'))
            <p class="alert alert-success">{{session()->get('success')}}</p>
        @elseif(session()->has('warning'))
            <p class="alert alert-warning">{{session()->get('warning')}}</p>
        @endif
        @yield('content')
    </div>
</div>
</body>
</html>
