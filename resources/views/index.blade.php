@extends('layouts.master')

@section('title', 'Главная')
@section('content')

        <h1>Все товары</h1>
        <form method="GET" action="{{route("main.index")}}">
        </form>
        <div class="row">
            @foreach($products as $product)
                @include('_partial.card', ['product'=>$product])
            @endforeach
        </div>

@endsection()
