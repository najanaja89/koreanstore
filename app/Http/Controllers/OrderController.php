<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    function index()
    {
        $orders = Order::where('status', 1)->get();

        return view('auth.orders.index', [
            'orders'=>$orders
        ]);
    }
    function show(Order $order)
    {
         return view('auth.orders.show', [
        'order'=>$order
    ]);
    }
}
