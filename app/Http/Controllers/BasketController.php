<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    function basket()
    {
        //dd("basket");
        $orderId = session('orderId');
        $order = $orderId;
        if (!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
        }
        return view('basket', [
            'order' => $order
        ]);
    }

    function order()
    {
        //dd("BasketController@order");
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('main.index');
        }
        $order = Order::find($orderId);
        return view('order', [
            'order' => $order
        ]);
    }

    function basketAdd($productId)
    {
        //dd('basketAdd');
        $orderId = session('orderId');
        if (is_null($orderId)) {
            //dd($productId);
            $order = Order::create();
            session(['orderId' => $order->id]);

        } else {
            $order = Order::find($orderId);
        }

        if ($order->products->contains($productId)) {
            $pivot = $order->products()->where('product_id', $productId)->first()->pivot;
            $pivot->count++;
            $pivot->update();
        } else {
            $order->products()->attach($productId);
        }

        if(Auth::check()){
            $order->user_id= Auth::id();
            $order->save();
        }

            $product = Product::find($productId);
        session()->flash('success', 'Добавлен товар ' .  $product->name);
        return redirect()->route('basket.basket');
    }

    function basketRemove($productId)
    {

        $orderId = session('orderId');
        $order = Order::find($orderId);
        if (is_null($orderId)) {
            return redirect()->route('basket.basket');
        }

        if ($order->products->contains($productId)) {
            $pivot = $order->products()->where('product_id', $productId)->first()->pivot;
            if ($pivot->count < 2) {
                $order->products()->detach($productId);
            } else {
                $pivot->count--;
                $pivot->update();
            }
        }
        $product = Product::find($productId);
        session()->flash('warning', 'Удален товар ' .  $product->name);
        return redirect()->route('basket.basket');
    }

    function confirm(Request $request)
    {
        //dd($request->all());
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('main.index');
        }
        $order = Order::find($orderId);
        $result = $order->saveOrder($request->name, $request->phone);
        if($result){
            session()->flash('success', 'Мы уже отправили Фродо и Сэма с вашим заказом');
        }else {
            session()->flash('warning', 'Ошибка');
        }
        return redirect()->route('main.index');
    }

}
