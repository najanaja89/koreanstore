<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeronalOrderController extends Controller
{
    function index()
    {

        //dd('PeronalOrderController');
        $orders = Order::where('user_id', Auth::user()->id)->get();

        return view('auth.orders.index', [
            'orders'=>$orders
        ]);
    }
    function show(Order $order)
    {
       if ( !Auth::user()->orders->contains($order))
       {
           return back();
       }
        return view('auth.orders.show', [
            'order'=>$order
        ]);
    }
}
