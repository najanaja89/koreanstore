<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class MainController extends Controller
{
    function index()
    {
        $products = Product::all();
        return view('index', [
            'products'=>$products
        ]);
    }

    function categories()
    {

        $categories = Category::all();
        return view('categories', [
            'categories' => $categories
        ]);
    }

    function product($category, $productCode = null)
    {
       $product = Product::where('code',$productCode)->first();
        return view('product', [
            'product' => $product
        ]);
    }


    function category($code)
    {
        $category = Category::where('code', $code)->first();
        return view('category', [
            'category' => $category
        ]);
    }


}
