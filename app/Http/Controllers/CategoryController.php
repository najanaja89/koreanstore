<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        return view('auth.categories.index', [
            'categories' => $categories
        ]);
    }


    public function create()
    {
        return view('auth.categories.form');
    }

    public function store(CategoryRequest $request)
    {
        $param = $request->all();
        unset($param['image']);
        if ($request->has('image')) {
            $path = $request->file('image')->store('categories');
            $param['image'] = $path;
        }
        Category::create($param);
        return redirect()->route('categories.index');
    }


    public function show(Category $category)
    {

        //dd($category);
        return view('auth.categories.show', [
            'category' => $category
        ]);
    }


    public function edit(Category $category)
    {
        return view('auth.categories.form', [
            'category' => $category
        ]);
    }


    public function update(CategoryRequest $request, Category $category)
    {
        $param = $request->all();
        unset($param['image']);
        if ($request->has('image')) {
            Storage::delete($category->image);
            $path = $request->file('image')->store('categories');
            $param['image'] = $path;
        }

        $category->update($param);
        return redirect()->route('categories.index');
    }


    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->route('categories.index');
    }
}
