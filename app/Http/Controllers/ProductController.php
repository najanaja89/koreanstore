<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public function index()
    {

        $products = Product::all();
        return view('auth.products.index', [
            'products' => $products
        ]);
    }


    public function create()
    {
        //dd('ProductController@create');
        $categories = Category::all();
        return view('auth.products.form', [
            'categories' => $categories
        ]);
    }


    public function store(ProductRequest $request)
    {
        //dd('ProductController@store');
        $param = $request->all();
        unset($param['image']);
        if ($request->has('image')) {
            $param['image'] =  $request->file('image')->store('products');
        }
        Product::create($param);

        return redirect()->route('products.index');
    }


    public function show(Product $product)
    {
        return view('auth.products.show', [
            'product' => $product
        ]);
    }


    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('auth.products.form', [
            'product' => $product,
            'categories' => $categories
        ]);
    }


    public function update(ProductRequest $request, Product $product)
    {
        $param = $request->all();
        unset($param['image']);
        if ($request->has('image')) {
            Storage::delete($product->image);
            $param['image'] = $request->file('image')->store('products');
        }

        $product->update($param);
        return redirect()->route('products.index');
    }


    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index');
    }
}
