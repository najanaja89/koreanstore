<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable =[
        'name','description', 'price', 'image', 'category_id', 'code'
    ];

    function category()
    {
        return $this->belongsTo(Category::class);
    }

    function getPriceForCount(){
        if(!is_null($this->pivot)){
            return $this->pivot->count * $this->price;
        }
        return $this->price;
    }
}
